# Exercices Formation C++20

Le code de base des exercices se trouve sous `src/practice/`. Chaque fichier `.cpp` est compilé en un executable du même nom.

Les corrections peuvent être trouvé parmis les fichiers sous `src/examples/`.

Suivre le [README](./README.md) pour le setup du repository. Plusieurs libs utilitaires sont compilés et peuvent être linké avec vos executable (voir [vcpkg_install.sh](./vcpkg_install.sh)). Si besoin modifier le fichier [CMakeLists.txt](./CMakeLists.txt) pour le link.

## Concepts

Une **interface** en programmation orientée objet représente des contraintes syntaxiques sur un type. La notion d'interface permet le polymorphisme qui consiste à l'implémentation d'algorithmes agissants sur des types quelconques à condition que ces types implémentes certaines interfaces.

En C++ il est possible de faire du polymorphisme à compile-time grâce aux templates: une fonction template prend en entrée des types quelquonque. Si les types de respectent pas la syntaxe utilisée par la syntaxe, le programme ne compile pas.

Avant le C++20, cette utilisation des templates fonctionne mais conduit à des messages d'erreurs compliqués à comprendre du point de vue du code client de la fonction template. Le C++20 introduit la possibilité de définir des **concepts** qui remplissent le rôle d'interface à compile-time. Une bibliothèque template utilisant des définitions de concepts sera ainsi beaucoup plus simple d'utilisation pour le développeur client de cette bibliothèque.

Le langage va plus loin via le mot clef `requires` permettant d'établir les contraintes syntaxiques sur un type. Ce mot clef permet la définition de concepts, mais plus généralement de prédicats évalués à compile-time. Ces prédicats peuvent être utilisés dans des expressions `contexpr` comme `if constexpr` ou `static_assert` pour choisir des branchements de code plus adaptés ou éviter des utilisations non prévus.

### Mise en bouche

(`src/practice/concepts/does_not_compile.cpp` à modifier.)

Voici un code qui ne compile pas:

```cpp
template <typename LhsT, typename RhsT>
constexpr auto add(LhsT &&lhs, RhsT &&rhs) {
  auto accum = std::forward<LhsT>(lhs);
  accum += std::forward<RhsT>(rhs);
  return accum;
}

struct T {
};

int main() {
  static_assert(add(T{}, T{}) == T{});
}
```

Ce code ne compile pas car le type `T` ne définit pas les bon opérateurs `+=` et `==` (en `constexpr`, pour que le `static_assert` puisse être évalué).

Avant de définir ces opérateur, l'objectif est de définir sur la fonction `add` des contraintes syntaxiques sur `LhsT` et `RhsT`.

- Utilisez `requires requires(LhsT lhs, RhsT rhs)` directement au niveau de la fonction `add` pour définir ces contraintes.
- Définir un concept avec ces contraintes et utilisez le.

Il faut ensuite ajouter à `T` les bon opérateurs.

- Définir `operator+=`
- Définir l'opérateur `==` en utilisant la définition par defaut du spaceship operator fournie par le compilateur.

### Choix à compile time d'une implémentation

(`src/practice/concepts/find_in_sorted_container.cpp` à modifier.)

Voici un code à trou dont l'objectif est de fournir une fonction pour vérifier si un élement est présent dans un conteneur trié:

```cpp
#include <cassert>
#include <list>
#include <vector>

template <typename ContainerT>
bool find_in_sorted_container(
    const ContainerT &container, const typename ContainerT::value_type &elmt
) {
  // A implementer
}

template <template <typename T> typename ContainerT> void test() {
  const ContainerT<int> empty{};
  const ContainerT<int> a{1, 8, 9, 15, 16};
  const ContainerT<int> b{9};
  const ContainerT<int> c{2, 5};

  assert(find_in_sorted_container(empty, 9) == false);
  assert(find_in_sorted_container(a, 9) == true);
  assert(find_in_sorted_container(b, 9) == true);
  assert(find_in_sorted_container(c, 9) == false);
}

int main() {
  test<std::vector>();
  test<std::list>();
  return 0;
}
```

- Ecrire le code de la fonction pour utiliser une approche par dichotomie lorsque l'opérateur `[]` est présent sur le type `ContainerT`, sinon une simple boucle.
- Ecrire un concept `ContainerOfComparable` qui vérifie qu'un type `ContainerT` implémente les élément utilisés dans la fonction, puis utiliser votre concept pour valider le type template de votre fonction.

## Ranges

La bibliothèque `ranges` a été introduite par Eric Niebler et permet de faire de la programmation fonctionelle plus facilement en C++.

Pour rappel la programmation fonctionnelle consiste à limiter les effets de bords, voir les supprimer completement, en ne manipulant que des constantes et expressions dérivées de ces constantes. En programmation fonctionnelle on évite la notion de variable dont la valeur peut changer. Cela conduit à privilégier la récursion sur l'itération, et à construire des expressions de fonctions qui s'enchainent.

L'objectif de la lib `ranges` initialement est de permettre l'application d'algorithmes directement sur des conteneurs plutot que sur les itérateurs `begin` et `end` de ces conteneurs. Cela donne accès aux expressions de fonctions qui s'enchainent, et d'évaluation dite paresseuse de ces expressions (qu'on appellera alors des `views`). La combinaison donne donc accès à une programmation fonctionelle efficace en terme de cout machine.

La lib `ranges` a commencée à être introduite dans la STL du C++20, mais beaucoup d'algorithmes ne sont pas encore intégré. Il est donc conseillé de toujours utiliser la lib originale de Niebler (`ranges-v3`) en transitionnant petit à petit sur les features supportées par la STL.

## Coroutines

L'idée de base des coroutines est de permettre au développeur d'écrire une fonction qui peut être mise en pause. Cette idée de base permet de construire des architectures de plus haut niveau permettant:

- les générateurs, des fonctions calculant des séquences potentiellement infinies ou imprédictible
  - suites mathématiques
  - vues itérables lazy
  - programmation réactive sur séquence d'évenements
- l'orchestration de thread
- l'attente non active sur fonctions d'input/output bas niveau
- l'execution de graphes de calculs sans préemption (concurrence collaborative)

Toutes ces applications peuvent être implémentées en C++20 avec les coroutines, mais avant de pouvoir s'y attaquer il faut comprendre le framework bas niveau qui nous est offert. Il est probable que le C++23 propose ces outils dans la STL mais à l'heure actuelle il faut les implémenter nous même.

### Mise en bouche

Le framework des coroutines n'est pas des plus simple à comprendre, et la manipulation est essentielle pour se familiariser. L'objectif de cet exercice est donc de réécrire les cas simples fournis en exemple, y ajouter des print, et comprendre comment les opérations s'orchestre dans le framework.

- `src/practice/coroutines/eager_future.cpp`
  - Une coroutine qui calcule la valeur d'un futur immédiatement
- `src/practice/coroutines/lazy_future.cpp`
  - Une coroutine qui calcule la valeur d'un futur à la demande
- `src/practice/coroutines/lazy_future_in_another_thread.cpp`
  - Une coroutine qui calcule la valeur d'un futur à la demande dans un autre thread.
- `src/practice/coroutines/infinite_data_stream.cpp`
  - Un futur qui implémente un générateur (itérateur asynchrone)
- `src/practice/coroutines/start_job.cpp`
  - Une coroutine simulant un job qui est arrété et peut être repris
- `src/practice/coroutines/start_job_with_automatic_resume.cpp`
  - Une coroutine simulant un job qui peut être executé directement ou non de manière aléatoire
- `src/practice/coroutines/start_job_with_automatic_resume_in_thread.cpp`
  - Une coroutine simulant un job qui peut être executé directement ou non de manière aléatoire mais qui se termine dans un thread

Vous pouvez consulter le fichier `src/practice/skeleton.cpp` pour un squelette des différents elements du framework.

### Requetes HTTP en asynchrone avec threads

L'I/O réseau est un cas d'application parfait des coroutines. Ce premier exercice propose l'implémentation d'une coroutine de requêtage réseau en utilisant la lib [`cpp-httplib`](https://github.com/yhirose/cpp-httplib).

Cette bibliothèque permet de requêter de manière bloquante: un appel à `client->Get(endpoint)` est bloquant jusqu'à ce que la requête soit terminée. Ce type d'execution est pratique mais il est nécessaire de multi-threader pour executer des requêtes en parallèle.

Nous avons vu précédement qu'il est possible de commencer une coroutine dans le thread principal pour terminer son execution dans un thread qui sera join lorsque le résultat est demandé. Ce type de coroutine a tout intérêt à être de type `eager` (aller le plus rapidement possible vers la création du thread pour rendre la main au thread principal).

L'objectif est de compléter le fichier `src/practice/http/async_thread_based_http_requests.cpp` pour implémenter une coroutine faisant ce travail. Le cas d'application est le requêtage de crypto-exchanges avec plusieurs requêtes en parallèle sur plusieurs exchanges.

Pour reference, voici un example d'utilisation de la lib cpp-httplib:

```cpp
#include <iostream>

#define CPPHTTPLIB_OPENSSL_SUPPORT
#include <httplib.h>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

int main() {
  httplib::Client cli("https://www.binance.com");
  const auto res = cli.Get("/fapi/v1/time");
  std::cout << res->status << std::endl;

  json result = json::parse(res->body);

  std::cout << result << std::endl;
  return 0;
}
```

### Gather de coroutines

Dans les autres langages il est possible de demander la complétion de plusieurs coroutines en concurrence. Par exemple python propose `await asyncio.gather(coro1, coro2, ...)`, et javascript propose `await Promise.all([promise1, promise2, ...])`.

L'objectif est d'implémenter une architecture de coroutines en C++20 permettant ce type de calcul. Attention, ce n'est pas trivial (de mon point de vue en tout cas) car il faut s'arranger pour qu'aucune coroutine ne prenne l'ascendant sur les autres. Si plusieurs `gather` doivent s'executer en concurrence, alors un `gather` doit toujours redonner la main à son parent entre chaque tour de boucle pour permettre une orchestration au tour par tour à haut niveau. Cela conduit à une notion de dépendance entre coroutines qui doit être gérée dans l'implémentation.

Vous devez compléter le fichier `src/practice/async_gather.cpp`. Les parties à compléter sont indiquées par `// To implement`. Vous ajouterez également des print pour montrer que l'application se séquence correctement.

Il y a probablement plusieurs implémentations possibles et très différentes, mais celle de la correction utilise une structure de donnée globale permettant de stocker les dépendances entre `gather()`. Le fichier de base contient déjà cette structure nommé `AsyncGraph`. Vous pouvez l'utiliser ou non.

### Requetes HTTP en asynchrone sans threads

Nous avons vu plus haut comment implémenter une coroutine de requête en utilisant un thread. Or il peut parfois être plus efficace de rester dans un seul thread pour faire nos requêtes, sans pour autant bloquer. Les appels système bas niveau sur socket permettent ce type d'implémentation. L'avantage des coroutines dans ce cas est de permettre une abstraction de plus haut niveau pour gérer plusieurs requêtes en concurrence grace au mécanisme de mise en pause.

L'objectif de cet exercice est donc d'exploiter les coroutines du C++20 pour transformer un code bas niveau executant ses requêtes HTTP dans une boucle active.

On combinera ensuite notre code HTTP asynchrone sans thread avec notre code de gather pour orchestrer un arbre de requêtage http en concurrence.

#### Le code bas niveau

Le fichier `src/exercices/http/https_base.cpp` contient le code d'un programme qui requete [l'API de l'exchange Coinbasepro](https://developers.coinbase.com/docs/exchange) en https.

La fonction principale est `RequestResponse get(const std::string &url)` qui implémente une boucle permettant de lire sur un socket des paquets de bytes de taille 64. Cette taille est volontairement faible pour avoir suffisement de tours de boucle pour nos tests. On l'augmentera par la suite pour effectuer beaucoup de requêtes en concurrence.

Commencez par lire ce code pour comprendre la logique d'attente active sur socket. Ou faudrait t'il introduire la méchanique de `co_await` ?

#### Implémentation de la coroutine

Dans le fichier `src/exercices/http/async_http_requets.cpp`, reprenez le code de `https_base.cpp` et transformez la fonction `get()` en une coroutine.

Dans le main, orchestrez plusieurs requêtes en parallèle sur l'url `https://www.binance.com/api/v1/exchangeInfo` (qui est normalement assez longue à fetch). Utilisez des print pour confirmer que les requêtes s'executent bien en concurrence.

#### Gather

Ajouter votre code de gather, avec les adaptation nécessaires, pour pouvoir executer un grand nombre de requêtes en parallèle. L'objectif est de réimplémenter le comportement de `async_thread_based_http_requetes.cpp` en utilisant vos coroutines http sans thread.
