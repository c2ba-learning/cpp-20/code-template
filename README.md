# C++20 Code Template

This repository provides an opiniated basic template for a C++20 project.

## Quickstart

You need CMake 3.22+ and a C++ compiler. The project have been tested on Windows with Visual Studio Community Edition 2019 16.11.11 and Archlinux with GCC 11.2.0

Run the following commands:

```bash
source .devenv/bash_init.sh
setup
```

This will:

- clone and build `vcpkg` to handle dependencies (installed in `.local/vcpkg`)
- build `gtest` dependency with vcpkg
- configure the cmake project to get the build tree `.local/cmake/build` (in debug mode)

After that you can build the project in debug mode:

```bash
cmake_build_debug
```

And you can run tests with

```bash
ctest_debug
```

You can also use the VSCode extension `ms-vscode.cmake-tools` to configure, build and debug from the UI with breakpoints.

Take a look at `.devenv/bash_init.sh` for other utility commands.

## Technical Stack

- Dependency management: [vcpkg - Open source C/C++ dependency manager from Microsoft](https://vcpkg.io/en/index.html)
- Build and test launcher: [CMake](https://cmake.org/)
- Code formatting: [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html)
- IDE: [Visual Studio Code](https://code.visualstudio.com/)
- CI/CD: [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)

## Customizing the project to your needs

Right now this project builds a single `hello_world` static library and its tests.

You can add new sources files under `src/` and update `CMakeLists.txt` to build what you need (executables, libraries, new tests).
