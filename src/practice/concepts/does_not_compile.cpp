#include <utility>

template <typename LhsT, typename RhsT>
constexpr auto add(LhsT &&lhs, RhsT &&rhs) {
  auto accum = std::forward<LhsT>(lhs);
  accum += std::forward<RhsT>(rhs);
  return accum;
}

struct T {};

int main() {
  // static_assert(add(T{}, T{}) == T{}); // Should compile after your changes
}
