#include <coroutine>
#include <exception>
#include <optional>

struct MyHandle {
  // This is not mandatory but most often our handle type have this:
  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;
  handle_type coro;

  MyHandle() = default;

  MyHandle(handle_type coro) : coro{coro} {}

  // Here we can define a custom interface for our handle type
  // this interface often provide utilities to resume the coroutine
  // or terminate its execution with a loop.
  int terminate() {
    while (!coro.done()) {
      coro.resume();
    }
    // When coro.done() is true, we can still access coro.promise()
    // ONLY IF the awaitable returned by promise_type::final_suspend()
    // is suspended. Otherwise the promise has been deleted so we should
    // have stored important data from it in MyHandle instead.
    if (coro.promise().except) {
      std::rethrow_exception(coro.promise().except);
    }
    return coro.promise().result;
  }

  // The promise_type inner type is mandatory and should define
  // an interface matching the coroutine framework
  struct promise_type {
    // If our coroutine can throw we store and exception in
    // unhandled_exception()
    std::exception_ptr except;
    int result;

    // get_return_object() is called by the compiler before the coroutine starts
    // and give to the client the instance returned by it.
    MyHandle get_return_object() {
      return MyHandle{handle_type::from_promise(*this)};
    }

    // return_value(x) is called when we do co_return x;
    // if our coroutine returns void, we implement return_void() instead
    void return_value(int result_) { result = result_; }

    // Optional, only for generators:
    // std::suspend_always yield_value(const T value);

    void unhandled_exception() { except = std::current_exception(); }

    // initial_suspend and final_suspend should return Awaitables
    std::suspend_always initial_suspend() const noexcept { return {}; }
    std::suspend_always final_suspend() const noexcept { return {}; }
  };
};

struct MyAwaitable {
  bool await_ready() const noexcept { return false; }
  void await_suspend(std::coroutine_handle<> coro) {
    // Note: coro is the handle of the coroutine that called
    // co_await *this;
    // the idea is that await_suspend should schedule coro for later
    // or let the client resume the coroutine.
    // await_suspend() can also return another std::coroutine_handle<>
    // to resume the execution of another coroutine.
  }
  int await_resume() { return 42; }
};

MyHandle my_coroutine() {
  // This function should use co_await, co_yield, and/or co_return.
  int value = co_await MyAwaitable{};
  co_return value;
}

int main() {
  MyHandle handle = my_coroutine();
  return handle.terminate();
}
