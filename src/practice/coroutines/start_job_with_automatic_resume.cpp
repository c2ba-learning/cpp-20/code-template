#include <coroutine>
#include <functional>
#include <iostream>
#include <random>

auto gen = std::bind_front(
    std::uniform_int_distribution(0, 1), std::default_random_engine(0)
);

struct MySuspendAlways {
  bool await_ready() const noexcept {
    const auto return_value = gen();
    std::cout << "MySuspendAlways::always_ready => " << return_value << '\n';
    return return_value; // randomly await or resume
  }

  void await_suspend(std::coroutine_handle<> handle) const noexcept {
    std::cout << "MySuspendAlways::await_suspend\n";
    // handle.resume();
  }

  void await_resume() const noexcept {
    std::cout << "MySuspendAlways::await_resume\n";
  }
};

struct Job {
  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;
  handle_type coro;
  Job(handle_type h) : coro{h} {}
  ~Job() {
    if (coro) {
      coro.destroy();
    }
  }

  struct promise_type {
    auto get_return_object() { return Job{handle_type::from_promise(*this)}; }
    MySuspendAlways initial_suspend() {
      std::cout << "Job prepared\n";
      return {};
    }
    std::suspend_always final_suspend() noexcept {
      std::cout << "Job finished\n";
      return {};
    }
    void return_void() {}
    void unhandled_exception(){};
  };
};

Job perform_job() {
  std::cout << "Entering perform_job\n";
  co_await std::suspend_never();
  std::cout << "Leaving perform_job\n";
}

int main() {
  std::cout << "Before jobs\n";

  std::cout << "Before job1\n";
  perform_job();

  std::cout << "Before job2\n";
  perform_job();

  std::cout << "Before job3\n";
  perform_job();

  std::cout << "Before job4\n";
  perform_job();

  std::cout << "After jobs\n";
}
