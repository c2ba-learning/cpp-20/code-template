#include <coroutine>
#include <iostream>
#include <memory>
#include <optional>

template <typename T> struct Generator {
  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;

  handle_type coro;
  Generator(handle_type h) : coro{h} { std::cout << "Generator::Generator\n"; }

  ~Generator() {
    std::cout << "Generator::~Generator\n";
    if (coro) {
      std::cout << "Destroying the coroutine\n";
      coro.destroy();
    }
  }

  Generator(const Generator &) = delete;
  Generator &operator=(const Generator &) = delete;
  Generator(Generator &&oth) noexcept : coro{oth.coro} { oth.coro = {}; }
  Generator &operator=(Generator &&oth) noexcept {
    coro = oth.coro;
    oth.coro = {};
    return *this;
  }

  T get_value() {
    std::cout << "Generator::get_value\n";
    return coro.promise().current_value;
  }

  bool next() {
    std::cout << "Generator::next before resume\n";
    coro.resume();
    std::cout << "Generator::next after resume\n";
    return !coro.done();
  }

  struct promise_type {
    promise_type() { std::cout << "promise_type::promise_type\n"; };
    ~promise_type() { std::cout << "promise_type::~promise_type\n"; };
    std::suspend_always initial_suspend() {
      std::cout << "promise_type::initial_suspend\n";
      return {};
    }
    std::suspend_always final_suspend() noexcept {
      std::cout << "promise_type::final_suspend\n";
      return {};
    }
    Generator<T> get_return_object() {
      std::cout << "promise_type::get_return_object\n";
      return Generator{handle_type::from_promise(*this)};
    }
    void return_void() { return; }

    std::suspend_always yield_value(const T value) {
      std::cout << "promise_type::yield_value\n";
      current_value = value;
      return {};
    }
    void unhandled_exception() { std::exit(1); }
    T current_value;
  };
};

Generator<int> get_generator(int start = 0, int step = 1) {
  std::cout << "entering get_generator\n";
  auto value = start;
  while (true) {
    std::cout << "get_generator: before co_yield\n";
    co_yield value;
    std::cout << "get_generator: after co_yield\n";
    value += step;
  }
}

int main() {
  Generator<int> gen = get_generator(0, 1);
  for (auto i = 0; i <= 2; ++i) {
    gen.next();
    std::cout << " " << gen.get_value() << '\n';
  }
  std::cout << '\n';
  return 0;
}
