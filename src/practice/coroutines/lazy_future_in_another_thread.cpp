#include <coroutine>
#include <iostream>
#include <memory>
#include <syncstream>
#include <thread>

auto out() {
  return std::osyncstream(std::cout)
      << "Thread " << std::this_thread::get_id() << ": ";
}

template <typename T> struct MyFuture {
  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;
  handle_type coro;
  MyFuture(handle_type coro) : coro{coro} { out() << "MyFuture::MyFuture\n"; }
  ~MyFuture() {
    if (coro) {
      out() << "destroying coroutine handle\n";
      coro.destroy();
    }
    out() << "MyFuture::~MyFuture\n";
  }
  T get() {
    out() << "MyFuture::get\n";
    out() << "resume starts\n";

    {
      auto thread = std::jthread([this]() { coro.resume(); });
    }

    out() << "resume ends\n";
    return coro.promise().result;
  }

  struct promise_type {
    T result;
    promise_type() { out() << "promise_type::promise_type\n"; }
    ~promise_type() { out() << "promise_type::~promise_type\n"; }
    MyFuture<T> get_return_object() {
      out() << "promise_type::get_return_object\n";
      return handle_type::from_promise(*this);
    }
    void return_value(T v) {
      out() << "promise_type::return_value\n";
      result = v;
    }
    std::suspend_always initial_suspend() {
      out() << "promise_type::initial_suspend\n";
      return {};
    }
    std::suspend_always
    final_suspend() noexcept { // suspend_always ensure the promise is not
                               // deleted at the end of the coroutine, so
                               // MyFuture<T> has access to this->result
      out() << "promise_type::final_suspend\n";
      return {};
    }
    void unhandled_exception() {
      out() << "promise_type::unhandled_exception\n";
      std::exit(1);
    }
  };
};

MyFuture<int> create_future() {
  out() << "starting create_future\n";
  co_return 2021;
  out() << "ending create_future\n";
}

int main() {
  out() << "starting main\n";
  auto fut = create_future();
  out() << "fut.get() = " << fut.get() << '\n';
  out() << "ending main\n";
  return 0;
}
