#include <cassert>
#include <list>
#include <vector>

template <typename ContainerT>
concept ContainerOfComparable = requires {
  typename ContainerT::value_type;
}
&&requires(ContainerT::value_type a, ContainerT::value_type b) {
  { a == b } -> std::convertible_to<bool>;
  { a < b } -> std::convertible_to<bool>;
};

template <typename ContainerT>
requires ContainerOfComparable<ContainerT>
bool find_in_sorted_container(
    const ContainerT &container, const typename ContainerT::value_type &elmt
) {
  if (container.empty()) {
    return false;
  }
  if constexpr (requires(ContainerT c, int i) { {c[i]}; }) {
    size_t first = 0;
    size_t last = container.size();
    size_t middle = (first + last) / 2;
    while (first != middle) {
      if (container[middle] == elmt) {
        return true;
      }
      if (elmt < container[middle]) {
        last = middle;
      } else {
        first = middle;
      }
      middle = (first + last) / 2;
    }
    return container[middle] == elmt;
  } else {
    for (const auto &x : container) {
      if (x == elmt) {
        return true;
      }
    }
    return false;
  }
}

template <template <typename T> typename ContainerT> void test() {
  const ContainerT<int> empty{};
  const ContainerT<int> a{1, 8, 9, 15, 16};
  const ContainerT<int> b{9};
  const ContainerT<int> c{2, 5};

  assert(find_in_sorted_container(empty, 9) == false);
  assert(find_in_sorted_container(a, 9) == true);
  assert(find_in_sorted_container(b, 9) == true);
  assert(find_in_sorted_container(c, 9) == false);
}

int main() {
  test<std::vector>();
  test<std::list>();
  return 0;
}
