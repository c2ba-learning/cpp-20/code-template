#include <concepts>

template <typename T>
concept BuiltinNumber =
    (std::integral<T> || std::floating_point<T>)&&!std::same_as<T, bool>;

template <typename T>
requires BuiltinNumber<T> T builtin_number_times_2(const T &value) {
  return 2 * value;
}

void test_number() {
  const int x = 42;
  builtin_number_times_2(x);

  const double y = 3.14;
  builtin_number_times_2(y);

  const bool b = true;
  // builtin_number_times_2(b); does not compile
}

template <typename T>
requires BuiltinNumber<T>
constexpr T builtin_number_times_2_optimized(const T &value) {
  if constexpr (std::integral<T>) {
    return value << 1;
  } else {
    return 2 * value;
  }
}

void test_number_at_compile_time() {
  static_assert(builtin_number_times_2_optimized(42) == 84);
  static_assert(builtin_number_times_2_optimized(42.0) == 84.0);
}
