#include <concepts>

template <typename T>
requires std::integral<T>
constexpr T gcd(T a, T b) {
  if (b == 0)
    return a;
  else
    return gcd(b, a % b);
}

template <typename T> constexpr T gcd1(T a, T b) requires std::integral<T> {
  if (b == 0)
    return a;
  else
    return gcd1(b, a % b);
}
template <std::integral T> constexpr T gcd2(T a, T b) {
  if (b == 0)
    return a;
  else
    return gcd2(b, a % b);
}

constexpr std::integral auto gcd3(std::integral auto a, std::integral auto b) {
  if (b == 0)
    return a;
  else
    return gcd3(b, a % b);
}

// const auto gcd4 =
//     [](std::integral auto a, std::integral auto b) -> std::integral auto{
//   if (b == 0)
//     return a;
//   else
//     return gcd4(b, a % b);
// };

constexpr auto gcd5(auto a, auto b) {
  if (b == 0)
    return a;
  else
    return gcd5(b, a % b);
}

constexpr auto gcd6(auto a, decltype(a) b) {
  if (b == 0)
    return a;
  else
    return gcd6(b, a % b);
}

void test_abbreviated_function_templates() {
  static_assert(gcd(100, 10) == 10);
  static_assert(gcd1(100, 10) == 10);
  static_assert(gcd2(100, 10) == 10);
  static_assert(gcd3(100, 10) == 10);
  // static_assert(gcd4(100, 10) == 10); not at compile time
  static_assert(gcd5(100, 10) == 10);
  static_assert(gcd6(100, 10) == 10);
}

#include <iostream>
#include <ranges>
#include <string>
#include <vector>

template <std::ranges::input_range Range>
requires std::ranges::view<Range>
class ContainerView : public std::ranges::view_interface<ContainerView<Range>> {
private:
  Range range_{};
  std::ranges::iterator_t<Range> begin_{std::begin(range_)};
  std::ranges::iterator_t<Range> end_{std::end(range_)};

public:
  ContainerView() = default;
  constexpr ContainerView(Range r)
      : range_(std::move(r)), begin_(std::begin(r)), end_(std::end(r)) {}
  constexpr auto begin() const { return begin_; }
  constexpr auto end() const { return end_; }
};
template <typename Range>
ContainerView(Range &&range) -> ContainerView<std::ranges::views::all_t<Range>>;

void abbreviated_function_templates() {

  std::vector<int> myVec{1, 2, 3, 4, 5, 6, 7, 8, 9};

  auto myContainerView = ContainerView(myVec);

  for (auto c : myContainerView)
    std::cout << c << " ";

  std::cout << '\n';

  for (auto i : std::views::reverse(ContainerView(myVec)))
    std::cout << i << ' ';

  std::cout << '\n';

  for (auto i : ContainerView(myVec) | std::views::reverse)
    std::cout << i << ' ';

  std::cout << '\n';

  std::cout << std::endl;

  std::string myStr = "Only for testing purpose.";

  auto myContainerView2 = ContainerView(myStr);

  for (auto c : myContainerView2)
    std::cout << c << " ";

  std::cout << '\n';

  for (auto i : std::views::reverse(ContainerView(myStr)))
    std::cout << i << ' ';

  std::cout << '\n';

  for (auto i : ContainerView(myStr) | std::views::reverse)
    std::cout << i << ' ';
  std::cout << '\n';
}
