#include <iterator>

struct Iterator {
  int *m_ptr;

  using iterator_category = std::forward_iterator_tag;
  using difference_type = std::ptrdiff_t;
  using value_type = int;
  using pointer = int *; // or also value_type*
  using reference = int &; // or also value_type&

  reference operator*() const { return *m_ptr; }
  // pointer operator->() { return m_ptr; }

  // Prefix increment
  Iterator &operator++() {
    m_ptr++;
    return *this;
  }

  // Postfix increment
  Iterator operator++(int) {
    Iterator tmp = *this;
    ++(*this);
    return tmp;
  }

  friend bool operator==(const Iterator &a, const Iterator &b) {
    return a.m_ptr == b.m_ptr;
  };
  friend bool operator!=(const Iterator &a, const Iterator &b) {
    return a.m_ptr != b.m_ptr;
  };
};

int main() {
  static_assert(std::forward_iterator<Iterator>);
  return 0;
}
