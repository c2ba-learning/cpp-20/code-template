#include <concepts>

struct X {
  int a;
};

template <typename T>
concept Test = requires(T x) {
  x.a;
};

template <typename T>
concept Addable = requires(T a, T b) {
  a + b;
};

template <typename T>
concept AddableAndProductable = requires(T a, T b) {
  a + b;
}
&&requires(T a, T b) { a *b; };

template <typename T>
concept Equal = requires(T a, T b) {
  { a == b } -> std::convertible_to<bool>;
  { a != b } -> std::convertible_to<bool>;
};

template <typename T> struct Other {};

template <typename T>
concept TypeRequirement = requires {
  typename T::value_type;
  typename Other<T>;
};

template <typename T>
requires requires(T a, T b) {
  { a == b } -> std::convertible_to<bool>;
  { a != b } -> std::convertible_to<bool>;
}
bool f(T a, T b) { return a == b; }

void test() { static_assert(Test<X>); }
