#include <type_traits>

template <typename T>
concept Integral = std::is_integral<T>::value;

template <typename T>
concept SignedIntegral = Integral<T> && std::is_signed<T>::value;

template <typename T>
concept UnsignedIntegral = Integral<T> && !SignedIntegral<T>;

void test_from_type_traits() {
  static_assert(Integral<int> && Integral<unsigned int> && Integral<char> && Integral<bool>);
  static_assert(SignedIntegral<int> && !SignedIntegral<unsigned int> && !Integral<float>);
  static_assert(UnsignedIntegral<unsigned char> && !UnsignedIntegral<char>);
}
