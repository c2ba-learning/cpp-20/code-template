#include <compare>
#include <concepts>
#include <utility>

// In C++20 we can use type... inside template<>
template <int...> struct sum_struct;

template <> struct sum_struct<> { static constexpr int value = 0; };

template <int i, int... tail> struct sum_struct<i, tail...> {
  static constexpr int value = i + sum_struct<tail...>::value;
};

template <int...> struct product_struct;

template <> struct product_struct<> { static constexpr int value = 1; };

template <int i, int... tail> struct product_struct<i, tail...> {
  static constexpr int value = i * product_struct<tail...>::value;
};

template <typename LhsT, typename RhsT>
requires requires(LhsT lhs, RhsT rhs) {
  { lhs += rhs } -> std::convertible_to<LhsT>;
}
constexpr auto add(LhsT &&lhs, RhsT &&rhs) {
  auto accum = std::forward<LhsT>(lhs);
  accum += std::forward<RhsT>(rhs);
  return accum;
}

struct T {
  constexpr auto operator<=>(const T &) const = default;
  constexpr T operator+=(const T &other) { return T{}; }
};

void compile_time_variadic_templates() {

  static_assert(sum_struct<1, 2, 3, 4, 5>::value == 1 + 2 + 3 + 4 + 5);
  static_assert(product_struct<1, 2, 3, 4, 5>::value == 1 * 2 * 3 * 4 * 5);

  static_assert(add(T{}, T{}) == T{});
}
