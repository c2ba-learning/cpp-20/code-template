template <typename T>
concept AcceptAll = true;

template <typename T>
concept AcceptNothing = false;

template <typename T>
requires AcceptAll<T>
void always_compile(const T &value) {}

template <typename T>
requires AcceptNothing<T>
void never_compile(const T &value) {}

void basic_concepts() {
  const auto the_answer = 42;

  always_compile(the_answer);

  // never_compile(the_answer);
  // error C7602: 'never_compile': the associated constraints are not satisfied
}
