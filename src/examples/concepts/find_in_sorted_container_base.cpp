#include <cassert>
#include <list>
#include <vector>

template <typename ContainerT>
bool find_in_sorted_container(
    const ContainerT &container, const typename ContainerT::value_type &elmt
) {
  // A implementer
}

template <template <typename T> typename ContainerT> void test() {
  const ContainerT<int> empty{};
  const ContainerT<int> a{1, 8, 9, 15, 16};
  const ContainerT<int> b{9};
  const ContainerT<int> c{2, 5};

  assert(find_in_sorted_container(empty, 9) == false);
  assert(find_in_sorted_container(a, 9) == true);
  assert(find_in_sorted_container(b, 9) == true);
  assert(find_in_sorted_container(c, 9) == false);
}

int main() {
  test<std::vector>();
  test<std::list>();
  return 0;
}
