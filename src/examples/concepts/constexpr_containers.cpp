#include <algorithm>
#include <vector>

// Not working with cl.exe 19.29.30141

constexpr int maxElement() {
  std::vector myVec = {1, 2, 4, 3};
  std::sort(myVec.begin(), myVec.end());
  return myVec.back();
}

void const_expr_containers() {
  constexpr int maxValue = maxElement();
  static_assert(maxValue == 4);
  constexpr int maxValue2 = [] {
    std::vector myVec = {1, 2, 4, 3};
    std::sort(myVec.begin(), myVec.end());
    return myVec.back();
  }();
  static_assert(maxValue2 == 4);
}
