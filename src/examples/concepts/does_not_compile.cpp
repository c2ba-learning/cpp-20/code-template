#include <compare>
#include <concepts>
#include <utility>

template <typename LhsT, typename RhsT>
constexpr auto add(LhsT &&lhs, RhsT &&rhs) {
  auto accum = std::forward<LhsT>(lhs);
  accum += std::forward<RhsT>(rhs);
  return accum;
}

struct T {};

void does_not_compile() {
  // static_assert(add(T{}, T{}) == T{});
}
