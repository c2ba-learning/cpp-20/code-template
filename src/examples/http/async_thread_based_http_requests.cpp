#include <coroutine>
#include <ranges>
#include <span>
#include <syncstream>
#include <thread>
#include <tuple>
#include <vector>

#include <fmt/core.h>

#define CPPHTTPLIB_OPENSSL_SUPPORT
#include <httplib.h>

#include <nlohmann/json.hpp>

#include "http_utils.hpp"

using json = nlohmann::json;

static constexpr size_t MAX_TICKERS = 10;

// Use this to output without data race on std::cout
auto out() {
  return std::osyncstream(std::cout)
      << "Thread " << std::this_thread::get_id() << ": ";
}

struct RequestHandle {
  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;

  handle_type coro;
  RequestHandle(handle_type coro) : coro{coro} {}

  json wait_result() {
    if (coro.promise().worker_thread.joinable()) {
      coro.promise().worker_thread.join();
    }
    if (coro.promise().except) {
      std::rethrow_exception(coro.promise().except);
    }
    return coro.promise().result;
  }

  struct promise_type {
    std::thread worker_thread;
    json result;
    std::exception_ptr except;

    auto get_return_object() {
      return RequestHandle{handle_type::from_promise(*this)};
    }
    std::suspend_never initial_suspend() { return {}; }
    std::suspend_always final_suspend() noexcept {
      // suspend_always allows to extend lifetime of promise after the end of
      // the code execution of the coroutine
      return {};
    }
    void return_value(json result_) { result = result_; }
    void unhandled_exception() { except = std::current_exception(); }
  };

  struct Awaitable {
    bool await_ready() const noexcept { return false; }

    void await_suspend(std::coroutine_handle<promise_type> coro
    ) const noexcept {
      coro.promise().worker_thread = std::thread([coro]() { coro.resume(); });
    }

    void await_resume() const noexcept {}
  };
};

RequestHandle get(std::string url) {
  co_await RequestHandle::Awaitable{};

  const auto parsed_url = parse_url(url);

  httplib::Client client(
      fmt::format("{}://{}", parsed_url.protocol, parsed_url.hostname)
  );
  out() << fmt::format("GET {}\n", url);
  const auto res = client.Get(parsed_url.endpoint.c_str());
  if (!res) {
    throw std::runtime_error(
        fmt::format("GET {} Unknown error {}", url, to_string(res.error()))
    );
  } else if (res->status != 200) {
    throw std::runtime_error(
        fmt::format("GET {} Error {} {}", url, res->status, res->body)
    );
  }
  co_return json::parse(res->body);
}

std::vector<json> gather(std::vector<RequestHandle> tasks) {
  std::vector<json> result;
  for (auto &task : tasks) {
    result.emplace_back(task.wait_result());
  }
  return result;
}

struct Ticker {
  std::string base_asset;
  std::string quote_asset;
  double bid_price;
  double ask_price;
};

std::vector<Ticker> fetch_coinbase_tickers() {
  const std::string products_url{"https://api.exchange.coinbase.com/products/"};

  const auto products = get(products_url).wait_result();
  std::vector<RequestHandle> ticker_requests;
  auto count = MAX_TICKERS;
  for (const auto &product : products) {
    const auto ticker_url = fmt::format(
        "https://api.exchange.coinbase.com/products/{}/ticker", product["id"]
    );
    std::cout << ticker_url << '\n';
    ticker_requests.emplace_back(get(ticker_url));
    if (--count == 0) {
      break;
    }
  }
  std::clog << "start gather coinbase\n";
  const auto ticker_responses = gather(ticker_requests);

  std::vector<Ticker> tickers;
  for (size_t product_idx = 0; product_idx < ticker_responses.size();
       ++product_idx) {
    const auto json_ticker = ticker_responses[product_idx];
    const auto &product = products[product_idx];
    std::cout << json_ticker << '\n';

    tickers.emplace_back(Ticker{
        .base_asset = product["base_currency"],
        .quote_asset = product["quote_currency"],
        .bid_price = std::atof(json_ticker["bid"].get<std::string>().c_str()),
        .ask_price = std::atof(json_ticker["ask"].get<std::string>().c_str())});
  }

  return tickers;
}

std::vector<Ticker> fetch_binance_tickers() {
  const std::string exchange_info_url{
      "https://www.binance.com/api/v1/exchangeInfo"};

  const auto exchange_info = get(exchange_info_url).wait_result();

  std::vector<RequestHandle> ticker_requests;
  auto count = MAX_TICKERS;
  for (const auto &symbol : exchange_info["symbols"]) {
    const auto ticker_url = fmt::format(
        "https://www.binance.com/api/v3/ticker/bookTicker?symbol={}",
        symbol["symbol"]
    );
    std::cout << ticker_url << '\n';
    ticker_requests.emplace_back(get(ticker_url));
    if (--count == 0) {
      break;
    }
  }

  std::clog << "start gather binance\n";
  const auto ticker_responses = gather(ticker_requests);

  std::vector<Ticker> tickers;
  for (size_t symbol_idx = 0; symbol_idx < ticker_responses.size();
       ++symbol_idx) {
    const auto json_ticker = ticker_responses[symbol_idx];
    const auto &symbol = exchange_info["symbols"][symbol_idx];
    std::cout << json_ticker << '\n';

    tickers.emplace_back(Ticker{
        .base_asset = symbol["baseAsset"],
        .quote_asset = symbol["quoteAsset"],
        .bid_price =
            std::atof(json_ticker["bidPrice"].get<std::string>().c_str()),
        .ask_price =
            std::atof(json_ticker["askPrice"].get<std::string>().c_str())});
  }

  return tickers;
}

int main(int argc, char *argv[]) {
  using namespace std::literals;
  const auto binance_thread = std::jthread{fetch_binance_tickers};
  // The first request exchangeInfo of binance is a bit long so we let it some
  // heads up
  std::this_thread::sleep_for(5s);
  const auto coinbase_thread = std::jthread{fetch_coinbase_tickers};

  return 0;
}
