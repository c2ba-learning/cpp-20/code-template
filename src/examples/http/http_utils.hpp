#pragma once

#if defined(_WIN32)
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment(lib, "ws2_32.lib")

#else
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#endif

#if defined(_WIN32)
#define ISVALIDSOCKET(s) ((s) != INVALID_SOCKET)
#define CLOSESOCKET(s) closesocket(s)
#define GETSOCKETERRNO() (WSAGetLastError())

#else
#define ISVALIDSOCKET(s) ((s) >= 0)
#define CLOSESOCKET(s) close(s)
#define SOCKET int
#define GETSOCKETERRNO() (errno)
#endif

#include <openssl/crypto.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/x509.h>

#include <iostream>
#include <regex>
#include <stdexcept>
#include <string>

#include <fmt/core.h>
#include <nlohmann/json.hpp>

static constexpr bool LOGGING_ENABLED = false;
static constexpr int LOGGING_LEVEL = 0;

struct UrlParseResult {
  std::string protocol;
  std::string hostname;
  std::string port;
  std::string endpoint;
};

inline UrlParseResult parse_url(std::string url) {
  static const std::regex url_regex{
      R"(^(([^:\/?#]+):)?(//([^\/?#:]*))((:([^\/]+)))?([^?#]*)(\?([^#]*))?(#(.*))?)"};

  std::smatch matches;
  std::regex_search(url, matches, url_regex);

  if constexpr (LOGGING_ENABLED && LOGGING_LEVEL == 1) {
    // Put if constexpr (true) to debug the regex
    for (size_t i = 0; i < matches.size(); ++i) {
      std::clog << i << ": '" << matches[i].str() << "'\n";
    }
  }

  const auto protocol = matches[2].str();
  const auto port = matches[7].str();

  return UrlParseResult{
      .protocol = protocol,
      .hostname = matches[4],
      .port = port != ""         ? port
          : (protocol == "http") ? "80"
                                 : "443",
      .endpoint = (std::string{matches[8]} + std::string{matches[9]})};
}

inline void send_request(
    SOCKET socket,
    SSL *ssl,
    std::string_view hostname,
    std::string_view port,
    std::string_view path
) {
  const auto req = fmt::format(
      "GET {} HTTP/1.1\r\n"
      "Host: {}:{}\r\n"
      "Connection: close\r\n"
      "User-Agent: c2ba 1.0\r\n"
      "\r\n",
      path,
      hostname,
      port
  );
  if (ssl) {
    SSL_write(ssl, req.data(), req.size());
  } else {
    send(socket, req.data(), req.size(), 0);
  }
}

inline SOCKET
connect_to_host(const std::string &hostname, const std::string &port) {
  struct addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_socktype = SOCK_STREAM;
  struct addrinfo *peer_address;
  if (getaddrinfo(hostname.c_str(), port.c_str(), &hints, &peer_address)) {
    throw std::runtime_error(
        fmt::format("getaddrinfo() failed. ({})", GETSOCKETERRNO())
    );
  }

  char address_buffer[100];
  char service_buffer[100];
  getnameinfo(
      peer_address->ai_addr,
      peer_address->ai_addrlen,
      address_buffer,
      sizeof(address_buffer),
      service_buffer,
      sizeof(service_buffer),
      NI_NUMERICHOST
  );

  SOCKET server;
  server = socket(
      peer_address->ai_family,
      peer_address->ai_socktype,
      peer_address->ai_protocol
  );
  if (!ISVALIDSOCKET(server)) {
    throw std::runtime_error(
        fmt::format("socket() failed. ({})", GETSOCKETERRNO())
    );
  }

  if (connect(server, peer_address->ai_addr, peer_address->ai_addrlen)) {
    throw std::runtime_error(
        fmt::format("connect() failed. ({})", GETSOCKETERRNO())
    );
  }
  freeaddrinfo(peer_address);

  return server;
}

inline std::vector<std::string>
split(std::string str, const std::string &separator) {
  std::vector<std::string> result;
  for (auto position = str.find(separator); position != std::string::npos;
       position = str.find(separator)) {
    const auto token = str.substr(0, position);
    result.push_back(token);
    str.erase(0, position + separator.length());
  }
  result.push_back(str);
  return result;
}

struct RequestResponse {
  std::string response_string;
  std::string status_line;
  int status;
  std::string status_description;
  std::string protocol_version_string;
  std::string body;
  std::unordered_map<std::string, std::string> headers;

  RequestResponse() = default;

  explicit RequestResponse(std::string response)
      : response_string{std::move(response)} {

    const auto lines = split(response_string, "\r\n");

    if (lines.size() < 2) {
      throw std::runtime_error("Invalid response string: " + response_string);
    }

    status_line = lines[0];
    const auto status_line_tokens = split(status_line, " ");
    protocol_version_string = status_line_tokens[0];
    status = std::atoi(status_line_tokens[1].c_str());
    status_description = status_line_tokens[2];

    auto header_idx = 1u;
    for (; lines[header_idx] != ""; ++header_idx) {
      const auto header_tokens = split(lines[header_idx], ": ");
      if (header_tokens.size() != 2) {
        throw std::runtime_error("Invalid header line: " + lines[header_idx]);
      }
      headers[header_tokens[0]] = header_tokens[1];
    }

    if (header_idx + 1 < lines.size()) {
      body = std::accumulate(
          std::begin(lines) + header_idx + 1, std::end(lines), std::string{}
      );
    }
  }

  void raise_for_status() const {
    if (status >= 400) {
      throw std::runtime_error("");
    }
  }

  nlohmann::json json() const { return nlohmann::json::parse(body); }
};

inline SSL_CTX *get_ssl_context() {
  static struct SSLContext {
    SSL_CTX *ctx = nullptr;
    SSLContext() {
      SSL_library_init();
      OpenSSL_add_all_algorithms();
      SSL_load_error_strings();

      ctx = SSL_CTX_new(TLS_client_method());
      if (!ctx) {
        throw std::runtime_error("SSL_CTX_new() failed.");
      }
    }

    ~SSLContext() {
      if (ctx) {
        SSL_CTX_free(ctx);
      }
    }
  } global_ssl_context;
  return global_ssl_context.ctx;
}

inline SSL *
get_ssl_if_required(const UrlParseResult &parsed_url, SOCKET socket) {
  if (parsed_url.protocol != "https") {
    return static_cast<SSL *>(nullptr);
  }
  SSL *ssl = SSL_new(get_ssl_context());
  if (!ssl) {
    throw std::runtime_error("SSL_new() failed.");
  }

  if (!SSL_set_tlsext_host_name(ssl, parsed_url.hostname.c_str())) {
    // Should use ERR_* API of OpenSSL to get exact error
    // (ERR_print_errors_fp)
    throw std::runtime_error("SSL_set_tlsext_host_name() failed.");
  }

  SSL_set_fd(ssl, socket);
  if (SSL_connect(ssl) == -1) {
    // Should use ERR_* API of OpenSSL to get exact error
    // (ERR_print_errors_fp)
    throw std::runtime_error("SSL_connect() failed.");
  }

  if constexpr (LOGGING_ENABLED) {
    std::clog << fmt::format("SSL/TLS using {}\n", SSL_get_cipher(ssl));
  }

  X509 *cert = SSL_get_peer_certificate(ssl);
  if (!cert) {
    throw std::runtime_error("SSL_get_peer_certificate() failed.");
  }

  char *tmp;
  if ((tmp = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0))) {
    if constexpr (LOGGING_ENABLED) {
      std::clog << fmt::format("subject: {}\n", tmp);
    }
    OPENSSL_free(tmp);
  }

  if ((tmp = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0))) {
    if constexpr (LOGGING_ENABLED) {
      std::clog << fmt::format("issuer: {}\n", tmp);
    }
    OPENSSL_free(tmp);
  }

  X509_free(cert);

  return ssl;
}

struct HttpSocketHandle {
  UrlParseResult parsed_url;
  SOCKET socket = connect_to_host(parsed_url.hostname, parsed_url.port);
  SSL *ssl = get_ssl_if_required(parsed_url, socket);

  HttpSocketHandle(std::string url) : parsed_url{parse_url(url)} {
    send_request(
        socket, ssl, parsed_url.hostname, parsed_url.port, parsed_url.endpoint
    );
  }

  ~HttpSocketHandle() {
    if (ssl) {
      SSL_shutdown(ssl);
      SSL_free(ssl);
    }
    CLOSESOCKET(socket);
  }

  size_t read(char *buffer, int max_size) {
    if (ssl) {
      return (size_t)SSL_read(ssl, buffer, max_size);
    }
    return (size_t)recv(socket, buffer, max_size, 0);
  }
};

struct WSAContext {
  WSAContext() {
#if defined(_WIN32)
    WSADATA d;
    if (WSAStartup(MAKEWORD(2, 2), &d)) {
      throw std::runtime_error("Failed to initialize.");
    }
#endif
  }
  ~WSAContext() {
#if defined(_WIN32)
    WSACleanup();
#endif
  }
};
