#include <charconv>
#include <coroutine>
#include <cstdlib>
#include <iostream>
#include <numeric>
#include <optional>
#include <ranges>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "http_utils.hpp"

static constexpr size_t RESPONSE_SIZE = 2 << 15;

static constexpr size_t MAX_TICKERS = 10;

template <typename T> std::string get_coroutine_name(T &&handle) {
  void *addr;
  if constexpr (std::convertible_to<T, std::coroutine_handle<>>) {
    addr = handle.address();
  } else {
    addr = handle.coro.address();
  }
  static int next_name = 0;
  static std::unordered_map<void *, std::string> names;
  const auto it = names.find(addr);
  if (it != std::end(names)) {
    return (*it).second;
  }
  return names[addr] = std::to_string(next_name++);
}

struct RequestHandle {
  using value_type = RequestResponse;

  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;

  handle_type coro;
  RequestHandle(handle_type coro) : coro{coro} {}

  RequestHandle() = default;

  bool done() const { return coro.done(); }

  void resume() { coro.resume(); }

  void next() {
    if (!coro.done()) {
      coro.resume();
      if (coro.promise().except) {
        std::rethrow_exception(coro.promise().except);
      }
    }
  }

  const RequestResponse &get_result() const {
    if (!coro.done()) {
      throw std::runtime_error("Coroutine not done, cannot get result");
    }
    if (coro.promise().except) {
      std::rethrow_exception(coro.promise().except);
    }
    if (!coro.promise().response) {
      throw std::runtime_error("No result in promise");
    }
    return coro.promise().response.value();
  }

  const RequestResponse &wait_result() {
    while (!done()) {
      resume();
    }
    return get_result();
  }

  struct promise_type {
    std::exception_ptr except;
    std::optional<RequestResponse> response;
    RequestHandle get_return_object() {
      return RequestHandle{handle_type::from_promise(*this)};
    }

    void return_value(RequestResponse response_) { response = response_; }

    std::suspend_always initial_suspend() const noexcept { return {}; }

    std::suspend_always final_suspend() const noexcept { return {}; }

    void unhandled_exception() { except = std::current_exception(); }
  };
};

RequestHandle get(std::string url) {
  HttpSocketHandle socket_handle{url};

  auto offset = size_t{0};
  std::vector<char> buffer;
  while (1) {
    fd_set reads;
    FD_ZERO(&reads);
    FD_SET(socket_handle.socket, &reads);

    struct timeval timeout {
      .tv_sec = 0, .tv_usec = 200000
    };

    if (select(socket_handle.socket + 1, &reads, 0, 0, &timeout) < 0) {
      throw std::runtime_error(
          fmt::format("select() failed. ({})", GETSOCKETERRNO())
      );
    }

    if (FD_ISSET(socket_handle.socket, &reads)) { // Could be in co_await
      buffer.resize(buffer.size() + RESPONSE_SIZE);
      const auto bytes_received =
          socket_handle.read(buffer.data() + offset, RESPONSE_SIZE);

      if (bytes_received < 1) {
        if constexpr (LOGGING_ENABLED) {
          std::clog << "Connection closed by peer\n";
        }
        break;
      }
      if constexpr (LOGGING_ENABLED) {
        std::clog << fmt::format(
            "{} Received ({} bytes)\n", url, bytes_received
        );
      }
      offset += bytes_received;
    }
    co_await std::suspend_always{};
  }

  if constexpr (LOGGING_ENABLED) {
    std::clog << "Closing socket...\n";
  }

  if (offset >= buffer.size()) {
    buffer.resize(buffer.size() + 1);
  }
  buffer[offset] = '\0';

  co_return RequestResponse{buffer.data()};
}

struct AsyncGraph {
  std::unordered_map<void *, std::coroutine_handle<>> dependencies;

  void set_dependency(
      std::coroutine_handle<> target, std::coroutine_handle<> dependency
  ) {
    dependencies[target.address()] = dependency;
  }

  std::coroutine_handle<> get_dependency(std::coroutine_handle<> target) {
    const auto it = dependencies.find(target.address());
    if (it == end(dependencies)) {
      return {};
    }
    return (*it).second;
  }
};

AsyncGraph &get_async_graph() {
  static AsyncGraph graph;
  return graph;
}

template <typename T> struct GatherHandle {
  using value_type = std::vector<T>;

  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;

  handle_type coro;

  GatherHandle(handle_type coro) : coro{coro} {}

  GatherHandle() = default;

  bool done() const { return coro.done(); }

  void resume() { coro.resume(); }

  const value_type &get_result() const {
    if (!coro.done()) {
      throw std::runtime_error("Coroutine not done, cannot get result");
    }
    if (coro.promise().except) {
      std::rethrow_exception(coro.promise().except);
    }
    if (!coro.promise().result) {
      throw std::runtime_error("No result in promise");
    }
    return coro.promise().result.value();
  }

  const value_type &wait_result() const {
    while (!coro.done()) {
      coro.resume();
    }
    return get_result();
  }

  bool await_ready() const noexcept { return false; }

  void await_suspend(std::coroutine_handle<> coro_) noexcept {
    get_async_graph().set_dependency(coro_, coro);
  }

  const std::vector<T> &await_resume() noexcept { return wait_result(); }

  struct promise_type {
    std::exception_ptr except;
    std::optional<std::vector<T>> result;

    GatherHandle get_return_object() {
      return GatherHandle{handle_type::from_promise(*this)};
    }

    template <typename R> void return_value(R &&result_) {
      result = std::move(std::forward<R>(result_));
    }

    std::suspend_never initial_suspend() const noexcept { return {}; }

    std::suspend_always final_suspend() const noexcept { return {}; }

    void unhandled_exception() { except = std::current_exception(); }
  };
};

template <typename HandleType>
GatherHandle<typename HandleType::value_type>
gather(std::vector<HandleType> tasks, std::string tag) {
  using T = typename HandleType::value_type;
  auto original_tasks = tasks;
  std::unordered_map<void *, T> results;

  while (!tasks.empty()) {
    decltype(tasks) next_tasks;
    for (auto task : tasks) {
      if constexpr (LOGGING_ENABLED) {
        std::cout << tag << " Taking " << get_coroutine_name(task) << '\n';
      }
      if (!task.done()) {
        const auto current_gather_dep =
            get_async_graph().get_dependency(task.coro);
        if (current_gather_dep && !current_gather_dep.done()) {
          if constexpr (LOGGING_ENABLED) {
            std::cout << tag << " Dependency "
                      << get_coroutine_name(current_gather_dep) << '\n';
          }
          current_gather_dep.resume();
        } else {
          if constexpr (LOGGING_ENABLED) {
            std::cout << tag << " Resuming " << get_coroutine_name(task)
                      << '\n';
          }
          task.resume();
        }
      }
      if (!task.done()) {
        if constexpr (LOGGING_ENABLED) {
          std::cout << tag << " Next " << get_coroutine_name(task) << '\n';
        }
        next_tasks.emplace_back(task);
      } else {
        if constexpr (LOGGING_ENABLED) {
          std::cout << tag << " Done " << get_coroutine_name(task) << '\n';
        }
        results[task.coro.address()] = task.get_result();
      }
      co_await std::suspend_always{};
    }
    std::swap(tasks, next_tasks);
  }
  const auto result = original_tasks |
      std::views::transform([&](const auto &task) {
                        return results[task.coro.address()];
                      }) |
      std::views::common;
  co_return std::vector(result.begin(), result.end());
}

template <typename Range>
GatherHandle<typename std::ranges::range_value_t<Range>::value_type>
gather(Range &&r, std::string tag) {
  using T = typename std::ranges::range_value_t<Range>;
  const auto x = r | std::views::common;
  co_return co_await gather(std::vector<T>(x.begin(), x.end()), tag);
}

struct Ticker {
  std::string base_asset;
  std::string quote_asset;
  double bid_price;
  double ask_price;
};

GatherHandle<Ticker> fetch_coinbase_tickers() {
  const std::string products_url{"https://api.exchange.coinbase.com/products/"};

  const auto products = get(products_url).wait_result().json();
  std::vector<RequestHandle> ticker_requests;
  int count = MAX_TICKERS;
  for (const auto &product : products) {
    const auto ticker_url = fmt::format(
        "https://api.exchange.coinbase.com/products/{}/ticker", product["id"]
    );
    std::cout << ticker_url << '\n';
    ticker_requests.emplace_back(get(ticker_url));
    if (--count == 0) {
      break;
    }
  }
  std::clog << "start gather coinbase\n";
  const auto ticker_responses =
      co_await gather(ticker_requests, "fetch_coinbase_tickers");

  std::vector<Ticker> tickers;
  for (size_t product_idx = 0; product_idx < ticker_responses.size();
       ++product_idx) {
    const auto json_ticker = ticker_responses[product_idx].json();
    const auto &product = products[product_idx];
    std::cout << json_ticker << '\n';

    tickers.emplace_back(Ticker{
        .base_asset = product["base_currency"],
        .quote_asset = product["quote_currency"],
        .bid_price = std::atof(json_ticker["bid"].get<std::string>().c_str()),
        .ask_price = std::atof(json_ticker["ask"].get<std::string>().c_str())});
  }

  co_return tickers;
}

GatherHandle<Ticker> fetch_binance_tickers() {
  const std::string exchange_info_url{
      "https://www.binance.com/api/v1/exchangeInfo"};

  std::clog << "start exchange_info\n";

  // Non-blocking fetch:
  // const auto exchange_info = ((co_await gather(
  //                                 std::vector{get(exchange_info_url)},
  //                                 "fetch_binance_tickers_exchange_info"
  //                             ))[0])
  //                                .json();

  // Blocking exchange fetch:
  const auto exchange_info = get(exchange_info_url).wait_result().json();

  std::clog << "end exchange_info\n";

  std::vector<RequestHandle> ticker_requests;
  int count = MAX_TICKERS;
  for (const auto &symbol : exchange_info["symbols"]) {
    const auto ticker_url = fmt::format(
        "https://www.binance.com/api/v3/ticker/bookTicker?symbol={}",
        symbol["symbol"]
    );
    std::cout << ticker_url << '\n';
    ticker_requests.emplace_back(get(ticker_url));
    if (--count == 0) {
      break;
    }
  }

  std::clog << "start gather binance\n";
  const auto ticker_responses =
      co_await gather(ticker_requests, "fetch_binance_tickers");

  std::vector<Ticker> tickers;
  for (size_t symbol_idx = 0; symbol_idx < ticker_responses.size();
       ++symbol_idx) {
    const auto json_ticker = ticker_responses[symbol_idx].json();
    const auto &symbol = exchange_info["symbols"][symbol_idx];
    std::cout << json_ticker << '\n';

    tickers.emplace_back(Ticker{
        .base_asset = symbol["baseAsset"],
        .quote_asset = symbol["quoteAsset"],
        .bid_price =
            std::atof(json_ticker["bidPrice"].get<std::string>().c_str()),
        .ask_price =
            std::atof(json_ticker["askPrice"].get<std::string>().c_str())});
  }

  co_return tickers;
}

int main(int argc, char *argv[]) {
  WSAContext wsa_context;

  auto coinbase_handle_1 = fetch_coinbase_tickers();
  std::clog << "between\n";
  auto binance_handle_2 = fetch_binance_tickers();

  auto handle =
      gather(std::vector{coinbase_handle_1, binance_handle_2}, "main");
  handle.wait_result();

  return 0;
}
