#include "http_utils.hpp"

static constexpr size_t RESPONSE_SIZE = 64;

RequestResponse get(const std::string &url);

int main() {
  WSAContext wsa_context; // Required on win32 platform

  const std::string url{"https://api.exchange.coinbase.com/products/"};

  const auto response = get(url);
  std::cout << response.body << std::endl;

  return 0;
}

RequestResponse get(const std::string &url) {
  HttpSocketHandle socket_handle{url};

  auto offset = size_t{0};
  std::vector<char> buffer;
  while (1) {
    fd_set reads;
    FD_ZERO(&reads);
    FD_SET(socket_handle.socket, &reads);

    struct timeval timeout {
      .tv_sec = 0, .tv_usec = 200000
    };

    if (select(socket_handle.socket + 1, &reads, 0, 0, &timeout) < 0) {
      throw std::runtime_error(
          fmt::format("select() failed. ({})", GETSOCKETERRNO())
      );
    }

    if (FD_ISSET(socket_handle.socket, &reads)) {
      buffer.resize(buffer.size() + RESPONSE_SIZE);
      const auto bytes_received =
          socket_handle.read(buffer.data() + offset, RESPONSE_SIZE);

      if (bytes_received < 1) {
        if constexpr (LOGGING_ENABLED) {
          std::clog << "Connection closed by peer\n";
        }
        break;
      }
      if constexpr (LOGGING_ENABLED) {
        std::clog << fmt::format(
            "{} Received ({} bytes)\n", url, bytes_received
        );
      }
      offset += bytes_received;
    }
  }

  if constexpr (LOGGING_ENABLED) {
    std::clog << "Closing socket...\n";
  }

  if (offset >= buffer.size()) {
    buffer.resize(buffer.size() + 1);
  }
  buffer[offset] = '\0';

  return RequestResponse{buffer.data()};
}
