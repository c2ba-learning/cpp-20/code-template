#include <iostream>

#define CPPHTTPLIB_OPENSSL_SUPPORT
#include <httplib.h>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

int main() {
  httplib::Client cli("https://www.binance.com");
  const auto res = cli.Get("/fapi/v1/time");
  std::cout << res->status << std::endl;

  json result = json::parse(res->body);

  std::cout << result << std::endl;
  return 0;
}
