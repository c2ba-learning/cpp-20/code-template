#include <chrono>
#include <coroutine>
#include <iostream>
#include <syncstream>
#include <thread>

using namespace std::chrono_literals;

auto out() {
  return std::osyncstream(std::cout)
      << "Thread " << std::this_thread::get_id() << ": ";
}

class Event {
public:
  void notify() noexcept {
    auto coro_ = coro.load();
    if (coro_) {
      out() << "Have a waiter, resuming the coroutine\n";
      notified = true;
      coro_.resume();
    } else {
      notified = false;
      out() << "Have no waiter, doing nothing\n";
    }
  };

  // There are essentially two ways to get an Awaiter.
  // - A co_await operator is defined.
  // - The Awaitable becomes the Awaiter.
  // Here we implement the Awaiter concept, so `co_await event` will result in
  // event becoming the awaiter. An altervative could be to implement operator
  // co_await() and return a class that implement the Awaiter concept.
  bool await_ready() const {
    out() << "await_ready called\n";
    if (coro.load()) {
      throw std::runtime_error("More than one waiter is not valid");
    }
    out() << "await_ready ok\n";
    return notified; // false => suspend; true => execute the coroutine
  }

  bool await_suspend(std::coroutine_handle<> coro_) noexcept {
    out() << "await_suspend called\n";
    if (notified) {
      out() << "already notified\n";
      return false;
    }
    coro.store(coro_);
    return true;
  }

  void await_resume() noexcept {}

private:
  std::atomic<bool> notified{false};
  std::atomic<std::coroutine_handle<>> coro;
};

struct Task {
  // When the task does not need access to the coroutine, no need to store a
  // handle.
  // Pausing and resuming is done through the Event, that will have the
  // coroutine handle thanks to await_suspend(handle)
  struct promise_type {
    Task get_return_object() { return {}; }
    std::suspend_never initial_suspend() {
      out() << "Preparing coroutine" << '\n';
      return {};
    }
    std::suspend_never final_suspend() noexcept {
      out() << "Terminating coroutine" << '\n';
      return {};
    }
    void return_void() {}
    void unhandled_exception() {}
  };
};

Task receiver(Event &event) {
  out() << "Starting receiver\n";
  auto start = std::chrono::high_resolution_clock::now();
  co_await event; // event.await_suspend() is called
  out() << "Got the notification !\n";
  auto end = std::chrono::high_resolution_clock::now();
  const std::chrono::duration<double> elapsed = end - start;
  out() << "Waited " << elapsed.count() << " seconds.\n";
}

int main() {
  out() << '\n';
  out() << "Notification before waiting\n";

  Event event0{};
  event0.notify();
  auto task = receiver(event0);
  out() << "Coming back to main()\n";
  event0.notify();
  out() << "\n";

  Event event1{};
  auto sender_thread1 = std::thread([&event1] { event1.notify(); });
  auto receiver_thread1 = std::thread(
      receiver, // A std::thread can run a coroutine
      std::ref(event1)
  );

  // Note: if sender_thread is executed before, then receiver_thread has not the
  // time to subscribe to the event and is never notified

  sender_thread1.join();
  receiver_thread1.join();

  out() << '\n';
  out() << "Notification after 2 seconds waiting\n";

  // Here the 2 secs are enough to subscribe

  Event event2{};
  auto receiver_thread2 = std::thread(receiver, std::ref(event2));
  auto sender_thread2 = std::thread([&event2] {
    out() << "Waiting before notif...\n";
    std::this_thread::sleep_for(2s);
    out() << "Notifying now !\n";
    event2.notify();
  });

  for (auto &thread : {std::ref(receiver_thread2), std::ref(sender_thread2)}) {
    out() << "Joining thread " << thread.get().get_id() << '\n';
    thread.get().join();
  }

  out() << '\n';

  return 0;
}
