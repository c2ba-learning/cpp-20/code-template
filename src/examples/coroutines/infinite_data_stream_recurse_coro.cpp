#include <coroutine>
#include <iostream>
#include <memory>
#include <optional>

template <typename T> struct Generator {
  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;

  handle_type coro;
  Generator(handle_type h) : coro{h} {}

  ~Generator() {
    if (coro) {
      coro.destroy();
    }
  }

  Generator(const Generator &) = delete;
  Generator &operator=(const Generator &) = delete;
  Generator(Generator &&oth) noexcept : coro{oth.coro} { oth.coro = {}; }
  Generator &operator=(Generator &&oth) noexcept {
    coro = oth.coro;
    oth.coro = {};
    return *this;
  }

  T get_value() { return coro.promise().current_value; }

  bool next() {
    coro.resume();
    return !coro.done();
  }

  struct promise_type {
    promise_type() = default;
    ~promise_type() = default;
    std::suspend_always initial_suspend() { return {}; }
    std::suspend_always final_suspend() noexcept { return {}; }
    Generator<T> get_return_object() {
      return Generator{handle_type::from_promise(*this)};
    }
    void return_void() { return; }
    std::suspend_always yield_value(const T value) {
      current_value = value;
      return {};
    }
    void unhandled_exception() { std::exit(1); }
    T current_value;
  };
};

Generator<int>
get_next(int start = 0, int step = 1, std::optional<int> end = {}) {
  auto value = start;
  while (!end || value < end) {
    if (step != 2) {
      auto gen = get_next(0, 2, 5);
      while (gen.next()) {
        co_yield 10 + gen.get_value();
      }
    }
    co_yield value;
    value += step;
  }
}

int main() {
  auto gen = get_next(0, 1, 5);

  gen.next();
  std::cout << " " << gen.get_value();

  gen.next();
  std::cout << " " << gen.get_value();

  gen.next();
  std::cout << " " << gen.get_value();

  gen.next();
  std::cout << " " << gen.get_value();

  // for (auto i = 0; gen.next() && i <= 10; ++i) {
  //   std::cout << " " << gen.get_value();
  // }
  std::cout << '\n';
  return 0;
}
