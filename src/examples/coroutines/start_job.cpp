#include <coroutine>
#include <iostream>

struct MySuspendAlways {
  bool await_ready() const noexcept {
    std::cout << "MySuspendAlways::await_ready\n";
    return false;
  }

  void await_suspend(std::coroutine_handle<>) const noexcept {
    std::cout << "MySuspendAlways::await_suspend\n";
  }

  int await_resume() const noexcept {
    std::cout << "MySuspendAlways::await_resume\n";
    return 24;
  }
};

struct MySuspendNever {
  bool await_ready() const noexcept {
    std::cout << "MySuspendNever::await_ready\n";
    return true;
  }

  void await_suspend(std::coroutine_handle<>) const noexcept {
    std::cout << "MySuspendNever::await_suspend\n";
  }

  int await_resume() const noexcept {
    std::cout << "MySuspendNever::await_resume\n";
    return 42;
  }
};

struct Job {
  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;
  handle_type coro;
  Job(handle_type h) : coro{h} {}
  ~Job() {
    if (coro) {
      coro.destroy();
    }
  }

  void start() {
    if (coro.done()) {
      std::cout << "Coroutine is done\n";
    } else {
      std::cout << "Resuming the coroutine\n";
      coro.resume();
      std::cout << "After resume\n";
    }
  }

  struct promise_type {
    auto get_return_object() { return Job{handle_type::from_promise(*this)}; }
    MySuspendAlways initial_suspend() {
      std::cout << "Job prepared\n";
      return {};
    }
    MySuspendAlways final_suspend() noexcept {
      std::cout << "Job finished\n";
      return {};
    }
    void return_void() {}
    void unhandled_exception(){};
  };
};

Job prepare_job() {
  std::cout << "Before co_await\n";
  const auto co_await_result = co_await MySuspendAlways{};
  std::cout << co_await_result << '\n';
  std::cout << "After co_await\n";
}

int main() {
  std::cout << "Before Job" << '\n';

  auto job = prepare_job();
  job.start();

  job.start();

  std::cout << "After Job" << '\n';
}
