#include <coroutine>
#include <functional>
#include <iostream>
#include <random>
#include <syncstream>
#include <thread>

auto gen = std::bind_front(
    std::uniform_int_distribution(0, 1), std::default_random_engine(0)
);

auto out() {
  return std::osyncstream(std::cout)
      << "Thread " << std::this_thread::get_id() << ": ";
}

struct MyAwaitable {
  std::jthread &outer_thread;
  bool await_ready() const noexcept {
    const auto is_ready = gen();
    out() << "MyAwaitable::await_ready => " << is_ready << '\n';
    return is_ready;
  }
  void await_suspend(std::coroutine_handle<> h) {
    out() << "MyAwaitable::await_suspend\n";
    // When await_read() returns false we end up here and start a new thread to
    // resume the coroutine
    outer_thread = std::jthread([h] { h.resume(); });
  }
  void await_resume() { out() << "MyAwaitable::await_resume\n"; }
};

struct Job {
  static inline int JOB_COUNTER = 1;
  Job() { ++JOB_COUNTER; }

  struct promise_type {
    int job_number{JOB_COUNTER};
    auto get_return_object() { return Job{}; }
    std::suspend_never initial_suspend() {
      out() << "Job " << job_number << " prepared\n";
      return {};
    }
    std::suspend_never final_suspend() noexcept {
      out() << "Job " << job_number << " finished\n";
      return {};
    }
    void return_void() {}
    void unhandled_exception(){};
  };
};

Job perform_job(std::jthread &thread) {
  out() << "Entering perform_job\n";
  co_await MyAwaitable{thread};
  out() << "Leaving perform_job\n";
}

int main() {
  out() << "Before jobs\n";

  std::vector<std::jthread> threads(8);
  for (auto &thread : threads) {
    perform_job(thread);
  }

  out() << "After jobs\n";
}
