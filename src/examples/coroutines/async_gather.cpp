#include <any>
#include <coroutine>
#include <iostream>
#include <optional>
#include <ranges>
#include <unordered_map>
#include <vector>

#include <fmt/core.h>

template <typename T> std::string get_coroutine_name(T &&handle) {
  void *addr;
  if constexpr (std::convertible_to<T, std::coroutine_handle<>>) {
    addr = handle.address();
  } else {
    addr = handle.coro.address();
  }
  static int next_name = 0;
  static std::unordered_map<void *, std::string> names;
  const auto it = names.find(addr);
  if (it != std::end(names)) {
    return (*it).second;
  }
  return names[addr] = std::to_string(next_name++);
}

template <typename T> struct TaskHandle {
  using value_type = T;

  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;

  handle_type coro;
  TaskHandle(handle_type coro) : coro{coro} {
    std::cout << "TaskHandle(" << get_coroutine_name(coro) << ")\n";
  }

  TaskHandle() = default;

  bool done() const { return coro.done(); }

  void resume() { coro.resume(); }

  const value_type &get_result() const {
    if (!coro.done()) {
      throw std::runtime_error("Coroutine not done, cannot get result");
    }
    if (coro.promise().except) {
      std::rethrow_exception(coro.promise().except);
    }
    if (!coro.promise().result) {
      throw std::runtime_error("No result in promise");
    }
    return coro.promise().result.value();
  }

  const value_type &wait_result() const {
    while (!coro.done()) {
      coro.resume();
    }
    return get_result();
  }

  struct promise_type {
    std::exception_ptr except;
    std::optional<T> result;
    TaskHandle get_return_object() {
      return TaskHandle{handle_type::from_promise(*this)};
    }

    template <typename R> void return_value(R &&result_) {
      result = std::move(std::forward<R>(result_));
    }

    std::suspend_never initial_suspend() const noexcept { return {}; }

    std::suspend_always final_suspend() const noexcept { return {}; }

    void unhandled_exception() { except = std::current_exception(); }
  };
};

struct AsyncGraph {
  std::unordered_map<void *, std::coroutine_handle<>> dependencies;

  void set_dependency(
      std::coroutine_handle<> target, std::coroutine_handle<> dependency
  ) {
    dependencies[target.address()] = dependency;
  }

  std::coroutine_handle<> get_dependency(std::coroutine_handle<> target) {
    const auto it = dependencies.find(target.address());
    if (it == end(dependencies)) {
      return {};
    }
    return (*it).second;
  }
};

AsyncGraph &get_async_graph() {
  static AsyncGraph graph;
  return graph;
}

template <typename T> struct GatherHandle {
  using value_type = std::vector<T>;

  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;

  handle_type coro;

  GatherHandle(handle_type coro) : coro{coro} {
    std::cout << "GatherHandle(" << get_coroutine_name(coro) << ")\n";
  }

  GatherHandle() = default;

  bool done() const { return coro.done(); }

  void resume() { coro.resume(); }

  const value_type &get_result() const {
    if (!coro.done()) {
      throw std::runtime_error("Coroutine not done, cannot get result");
    }
    if (coro.promise().except) {
      std::rethrow_exception(coro.promise().except);
    }
    if (!coro.promise().result) {
      throw std::runtime_error("No result in promise");
    }
    return coro.promise().result.value();
  }

  const value_type &wait_result() const {
    while (!coro.done()) {
      coro.resume();
    }
    return get_result();
  }

  bool await_ready() const noexcept {
    std::cout << "GatherHandle::await_ready (from "
              << get_coroutine_name(this->coro) << ")\n";
    return false;
  }

  void await_suspend(std::coroutine_handle<> coro_) noexcept {
    std::cout << "GatherHandle::await_suspend " << get_coroutine_name(coro)
              << " (from " << get_coroutine_name(this->coro) << ")"
              << "\n";
    get_async_graph().set_dependency(coro_, coro);
  }

  const std::vector<T> &await_resume() noexcept {
    std::cout << "GatherHandle::await_resume (from "
              << get_coroutine_name(this->coro) << ")\n";
    if (!this->coro.done()) {
      std::cout << "GatherHandle " << get_coroutine_name(this->coro)
                << " not done\n";
      this->wait_result();
    }
    return this->get_result();
  }

  struct promise_type {
    std::exception_ptr except;
    std::optional<std::vector<T>> result;

    GatherHandle get_return_object() {
      return GatherHandle{handle_type::from_promise(*this)};
    }

    template <typename R> void return_value(R &&result_) {
      result = std::move(std::forward<R>(result_));
    }

    std::suspend_never initial_suspend() const noexcept { return {}; }

    std::suspend_always final_suspend() const noexcept { return {}; }

    void unhandled_exception() { except = std::current_exception(); }
  };
};

template <typename HandleType>
GatherHandle<typename HandleType::value_type>
gather(std::vector<HandleType> tasks, std::string gather_id) {
  using T = typename HandleType::value_type;
  auto original_tasks = tasks;
  std::unordered_map<void *, T> results;

  std::cout << "gather_id = " << gather_id << " Gather tasks = ";
  for (auto t : tasks) {
    std::cout << get_coroutine_name(t) << " ";
  }
  std::cout << '\n';
  while (!tasks.empty()) {
    decltype(tasks) next_tasks;
    for (auto task : tasks) {
      if (!task.done()) {
        std::cout << "gather_id = " << gather_id << " Taking "
                  << get_coroutine_name(task) << '\n';
        const auto current_gather_dep =
            get_async_graph().get_dependency(task.coro);
        if (current_gather_dep && !current_gather_dep.done()) {
          std::cout << "gather_id = " << gather_id << " Resuming dependency "
                    << get_coroutine_name(current_gather_dep) << '\n';
          current_gather_dep.resume();
        } else {
          std::cout << "gather_id = " << gather_id << " Resuming "
                    << get_coroutine_name(task) << '\n';
          task.resume();
        }
      }
      if (!task.done()) {
        std::cout << "gather_id = " << gather_id << " Rescheduling "
                  << get_coroutine_name(task) << '\n';
        next_tasks.emplace_back(task);
      } else {
        std::cout << "gather_id = " << gather_id << " Done "
                  << get_coroutine_name(task) << '\n';
        results[task.coro.address()] = task.get_result();
      }
      co_await std::suspend_always{};
      std::cout << "gather_id = " << gather_id << " Back to gather" << '\n';
    }
    std::swap(tasks, next_tasks);
    std::cout << "gather_id = " << gather_id << " Still running tasks = ";
    for (auto t : tasks) {
      std::cout << get_coroutine_name(t) << ", ";
    }
    std::cout << '\n';
  }
  std::cout << "gather_id = " << gather_id << " End of gather\n";
  const auto result = original_tasks |
      std::views::transform([&](const auto &task) {
                        return results[task.coro.address()];
                      }) |
      std::views::common;
  co_return std::vector(result.begin(), result.end());
}

template <typename Range>
GatherHandle<typename std::ranges::range_value_t<Range>::value_type>
gather(Range &&r, std::string gather_id) {
  const auto x = r | std::views::common;
  co_return co_await gather(std::vector(x.begin(), x.end()), gather_id);
}

TaskHandle<int> times_two(int i) {
  auto result = i;
  std::cout << "times_two(" << i << ") step 1\n";
  co_await std::suspend_always{};
  result += i;
  std::cout << "times_two(" << i << ") step 2\n";
  co_await std::suspend_always{};
  std::cout << "times_two(" << i << ") step 3\n";
  co_return result;
}

TaskHandle<int> times_two_plus_one(int i) {
  auto result = i;
  std::cout << "times_two_plus_one(" << i << ") step 1\n";
  co_await std::suspend_always{};
  result += i;
  std::cout << "times_two_plus_one(" << i << ") step 2\n";
  co_await std::suspend_always{};
  result += 1;
  std::cout << "times_two_plus_one(" << i << ") step 3\n";
  co_await std::suspend_always{};
  std::cout << "times_two_plus_one(" << i << ") step 4\n";
  co_return result;
}

TaskHandle<std::vector<int>>
make_evens(std::vector<int> values, std::string id) {
  co_return co_await gather(
      values | std::views::transform(times_two), "make_evens " + id
  );
}

TaskHandle<std::vector<int>>
make_odds(std::vector<int> values, std::string id) {
  co_return co_await gather(
      values | std::views::transform(times_two_plus_one), "make_odds " + id
  );
}

TaskHandle<int> c(int x) { co_return x; }

TaskHandle<int> add(std::vector<TaskHandle<int>> nodes, std::string id) {
  const auto results = co_await gather(nodes, "add " + id);
  int result = 0;
  for (const auto &i : results) {
    result += i;
    co_await std::suspend_always{};
  }
  co_return result;
}

int main() {
  auto task1 = make_evens({1, 2, 3, 4}, "task1");
  auto task2 = make_odds({5, 6}, "task2");
  auto task3 = make_evens({1, 2, 3, 4}, "task3");
  auto task4 = make_odds({5, 6}, "task4");

  auto even_and_odds_1 = gather(std::vector{task1, task2}, "even_and_odds_1");
  auto even_and_odds_2 = gather(std::vector{task3, task4}, "even_and_odds_2");

  auto both_gather =
      gather(std::vector{even_and_odds_1, even_and_odds_2}, "both_gather");

  auto both_gather_result = both_gather.wait_result();

  for (auto &both_result : both_gather_result) {
    for (auto &result_vector : both_result) {
      for (auto i : result_vector) {
        std::cout << i << ", ";
      }
      std::cout << '\n';
    }
  }

  // auto sum_handle = add(std::vector{
  //     c(1), //
  //     add(std::vector{
  //         c(2), //
  //         c(3) //
  //     }), //
  //     add(std::vector{
  //         c(4), //
  //         add(std::vector{
  //             c(5), //
  //             c(6) //
  //         }), //
  //         c(7) //
  //     })});
  // std::cout << sum_handle.wait_result() << '\n';

  return 0;
}
