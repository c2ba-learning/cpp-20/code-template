#include <coroutine>
#include <iostream>
#include <memory>

template <typename T> struct MyFuture {
  struct promise_type;
  using handle_type = std::coroutine_handle<promise_type>;
  handle_type coro;
  MyFuture(handle_type coro) : coro{coro} {
    std::cout << "MyFuture::MyFuture\n";
  }
  ~MyFuture() {
    if (coro) {
      std::cout << "destroying coroutine handle\n";
      coro.destroy();
    }
    std::cout << "MyFuture::~MyFuture\n";
  }
  T get() {
    std::cout << "MyFuture::get\n";
    std::cout << "resume starts\n";
    coro.resume();
    std::cout << "resume ends\n";
    return coro.promise().result;
  }

  struct promise_type {
    T result;
    promise_type() { std::cout << "promise_type::promise_type\n"; }
    ~promise_type() { std::cout << "promise_type::~promise_type\n"; }
    MyFuture<T> get_return_object() {
      std::cout << "promise_type::get_return_object\n";
      return handle_type::from_promise(*this);
    }
    void return_value(T v) {
      std::cout << "promise_type::return_value\n";
      result = v;
    }
    std::suspend_always initial_suspend() {
      std::cout << "promise_type::initial_suspend\n";
      return {};
    }
    std::suspend_always
    final_suspend() noexcept { // suspend_always ensure the promise is not
                               // deleted at the end of the coroutine, so
                               // MyFuture<T> has access to this->result
      std::cout << "promise_type::final_suspend\n";
      return {};
    }
    void unhandled_exception() {
      std::cout << "promise_type::unhandled_exception\n";
      std::exit(1);
    }
  };
};

MyFuture<int> create_future() {
  std::cout << "starting create_future\n";
  co_return 2021;
  std::cout << "ending create_future\n";
}

int main() {
  std::cout << "starting main\n";
  auto fut = create_future();
  std::cout << "fut.get() = " << fut.get() << '\n';
  std::cout << "ending main\n";
  return 0;
}
