#include <chrono>
#include <iostream>
#include <sstream>

#include <fmt/chrono.h>
#include <fmt/core.h>

int main() {
  std::time_t t = std::time(nullptr);

  // Prints "The date is 2020-11-07." (with the current date):
  fmt::print("The date is {:%F %X %Z}.\n", fmt::localtime(t));

  using namespace std::literals;

  // Prints "Default format: 42s 100ms":
  fmt::print("Default format: {} {}\n", 42s, 100ms);

  // Prints "strftime-like format: 03:15:30":
  fmt::print("strftime-like format: {:%H:%M:%S}\n", 3h + 15min + 30s);

  fmt::print("{:%F %X}\n", std::chrono::system_clock::now());

  std::chrono::system_clock::time_point time_point0;
  std::istringstream{"2021-8-5 17:00:00"} >>
      std::chrono::parse("%F %T"s, time_point0);

  fmt::print("{:%F %X}\n", time_point0);

  std::chrono::utc_clock::time_point time_point1;
  std::istringstream{"2021-8-5 17:00:00"} >>
      std::chrono::parse("%F %T"s, time_point1);

  // Does not work out of the box: fmt has no formatter for
  // std::chrono::utc_clock::time_point:

  // fmt::print("{:%F %X}\n", time_point1);

  using TimePoint = std::chrono::utc_time<std::chrono::utc_clock::duration>;

  TimePoint time_point;
  std::istringstream{"2021-8-5 17:00:00"} >>
      std::chrono::parse("%F %T"s, time_point);

  const auto time_point_utc =
      std::chrono::clock_cast<std::chrono::utc_clock>(time_point);

  // Produce a segfault, fmt does not handle durations with utc time_points
  std::cout << "utc time: "
            << fmt::format("{:%F %X %Z}", time_point_utc.time_since_epoch())
            << '\n';

  return 0;
}
