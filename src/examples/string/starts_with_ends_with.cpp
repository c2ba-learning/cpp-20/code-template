#include <iostream>
#include <string>

int main() {
  std::cout << std::boolalpha;
  std::string hello_world{"Hello World !"};
  std::cout << hello_world.starts_with("Hello") << '\n';
  std::cout << hello_world.ends_with("World !") << '\n';
  return 0;
}
