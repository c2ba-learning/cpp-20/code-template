#include <ranges>
#include <vector>

template <std::ranges::range R> auto to_vector(R &&r) {
  std::vector<std::ranges::range_value_t<R>> v;

  // if we can get a size, reserve that much
  if constexpr (requires { std::ranges::size(r); }) {
    v.reserve(std::ranges::size(r));
  }

  // push all the elements
  for (auto &&e : r) {
    v.emplace_back(static_cast<decltype(e) &&>(e));
  }

  return v;
}

template <std::ranges::range R> auto to_vector_less_optimized(R &&r) {
  auto r_common = r | std::views::common;
  return std::vector(r_common.begin(), r_common.end());
}
