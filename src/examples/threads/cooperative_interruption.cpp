#include <thread>

#include <fmt/core.h>
#include <fmt/ostream.h>

int main() {
  using namespace std::literals;

  std::jthread my_thread{[](std::stop_token stop_token) {
    while (!stop_token.stop_requested()) {
      fmt::print("Thread {}: in my loop\n", std::this_thread::get_id());
      std::this_thread::sleep_for(1s);
    }
    fmt::print("Thread {}: buy !\n", std::this_thread::get_id());
  }};

  std::this_thread::sleep_for(2s);
  my_thread.request_stop();

  return 0;
}
