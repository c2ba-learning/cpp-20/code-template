#include <atomic>
#include <iostream>
#include <thread>

#include <fmt/core.h>

static constinit auto version = 20u;

int main(int argc, const char *argv[]) {
  using namespace std::literals;
  std::atomic<bool> flag1, flag2;
  std::jthread thread([&]() {
    // std::this_thread::sleep_for(5s);
    while (!flag1.load()) {
    };
    std::cout << fmt::format("C++{}", version);
    flag2.store(true);
  });
  std::cout << "Hello ";
  flag1.store(true);
  std::this_thread::sleep_for(5s);
  flag1.store(false);
  while (!flag2.load()) {
  };
  std::cout << " !\n";
  return 0;
}
