#include <gtest/gtest.h>

#include "hello_world.hpp"

class HelloWorldTest: public ::testing::Test {
  protected:
  // You can remove any or all of the following functions if their bodies would
  // be empty.

  HelloWorldTest() {
     // You can do set-up work for each test here.
  }

  ~HelloWorldTest() override {
     // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  void SetUp() override {
     // Code here will be called immediately after the constructor (right
     // before each test).
  }

  void TearDown() override {
     // Code here will be called immediately after each test (right
     // before the destructor).
  }

  // Class members declared here can be used by all tests in the test suite
  // for HelloWorld.
};

TEST_F(HelloWorldTest, HelloWorldWithEmptyString) {
  const auto str = get_hello_world("");
  EXPECT_EQ(str, "Hello World !");
}

TEST_F(HelloWorldTest, HelloWorldWithMyName) {
  const auto str = get_hello_world("Laurent");
  EXPECT_EQ(str, "Hello World Laurent !");
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
