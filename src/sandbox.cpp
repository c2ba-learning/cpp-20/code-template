#include <any>
#include <iostream>
#include <optional>
#include <tuple>
#include <variant>
#include <vector>

struct MyString {
  std::string s;

  MyString() = default;

  MyString(std::string s2) : s{std::move(s2)} {}
};

struct Array {
  int *tab = nullptr;

  Array() : tab{new int[1000000]} {}

  ~Array() {
    if (tab) {
      delete[] tab;
    }
  }

  Array(const Array &other) {
    //
  }

  Array &operator=(const Array &other) {
    //
    return *this;
  }

  Array(Array &&other) : tab(other.tab) { other.tab = nullptr; }
  Array &operator=(Array &&other) { std::swap(tab, other.tab); }
};

void toto(Array array) {}

Array g() {
  Array a;
  return a;
}

constexpr std::vector<int> f() {
  const auto x = std::vector<int>{1, 2, 3, 4};
  return x;
}

std::unique_ptr<int> h() { return std::make_unique<int>(5); }

// RAII Resource Acquisition is Initialization
// std::auto_ptr

std::string create_str() { return "lol"; }

template <typename T1, typename T2> auto add(T1 x, T2 y) -> decltype(x + y) {
  return x + y;
}

// std::tuple<int, float, std::string> make_my_tuple() {
//   return std::make_tuple(5, 12.5, "toto");
// }

// template <typename... T> auto add(T... args) { return args + ... + 0; }

int main() {
  // std::initializer_list
  int t[] = {1, 2, 3, 4};
  std::vector<int> x(10);
  std::vector<int> y{10};

  std::cout << x.size() << std::endl;
  std::cout << y.size() << std::endl;

  std::vector<int> f{1, 2, 3};
  std::cout << f.size() << std::endl;

  auto toto = add(5, 2.5);

  auto functor = [&](auto x) { return x; };
  functor(5);
  functor("toto");

  // std::cout << add(5, 12.5, 0, 5u) << std::endl;

  // const auto my_tuple = make_my_tuple();

  // std::get<0>(my_tuple);
  // std::get<1>(my_tuple);
  // std::get<2>(my_tuple);

  // auto [my_int, my_float, my_str] = make_my_tuple();

  std::any a1 = 5;
  a1 = std::string("lol");
  a1 = 12.5;

  std::optional<int> a2 = 5;

  if (a2) {
  }

  std::variant<int, std::string> a3{5};
  a3 = "toto";

  return 0;
}
