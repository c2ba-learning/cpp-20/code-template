#include <string>

std::string get_hello_world(std::string_view who) {
  if (who == "") {
    return "Hello World !";
  }
  return std::string("Hello World ") + who.data() + " !";
}
